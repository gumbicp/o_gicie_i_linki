#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  vcsconstans.py
#
#
#
# Copyright 2014 gumbi <pracacp@gmaill.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
__author__ = 'gumbicp'

###########################################################################################################
################################################# Git #####################################################
###########################################################################################################

# ----------------------------------------------  GIT HELP FLAGS ------------------------------------------
# -a, --all
#        Prints all the available commands on the standard output. This option supersedes any other option.
#
#    -i, --info
#        Display manual page for the command in the info format. The info program will be used for that
#        purpose.
#
#    -m, --man
#        Display manual page for the command in the man format. This option may be used to override a value
#        set in the help.format configuration variable.
#
#        By default the man program will be used to display the manual page, but the man.viewer configuration
#        variable may be used to choose other display programs (see below).
#
#    -w, --web
#        Display manual page for the command in the web (HTML) format. A web browser will be used for that
#        purpose.
#
#        The web browser can be specified using the configuration variable help.browser, or web.browser if the
#        former is not set. If none of these config variables is set, the git web--browse helper script
#        (called by git help) will pick a suitable default. See git-web--browse(1) for more information about
#        this.
SET_HELP_FLAGS = {'-a', '--all', '-i', '--info', '-m,', '--man', '-w', '--web'}

# ----------------------------------------------  GIT ADD FLAGS ------------------------------------------

SET_ADD_FLAGS = {'-A', '--all', '--update', '-u', '.', '*', '-n', '--dry-run'}
# ----------------------------------------------  DICT FOR GIT COMMANDS ---------------------------------------
COMMANDS_GIT = {'help': SET_HELP_FLAGS, 'add': SET_ADD_FLAGS}
#, diff, rebase, am, fetch, reflog
# annotate            filter-branch       relink
# apply               format-patch        remote
# archive             fsck                repack
# bisect              gc                  replace
# blame               get-tar-commit-id   request-pull
# branch              grep                reset
# bundle              gui                 revert
# checkout            help                rm
# cherry-pick         imap-send           send-email
# cherry              init                shortlog
# citool              instaweb            show-branch
# clean               log                 show
# clone               mergetool           stage
# commit              merge               stash
# config              mv                  status
# credential-cache    name-rev            submodule
# credential-store    notes               svn
# describe            pull                tag
# difftool            push                whatchanged

###########################################################################################################
################################################# Mercurial ###############################################
###########################################################################################################