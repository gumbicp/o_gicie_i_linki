#!/usr/bin/env python3.3
#-*- coding: utf-8 -*-
#
# constans.py
#
#   Plik stałych dla gitmanager.py
#

__author__ = 'Gumbicp'

SEP = ' '
# -------- git -------------
ADD = "add"
ARCHIVE = 'archive'
BRANCH = "branch"
CLONE = 'clone'
CONFIG = 'config'
COMMIT = 'commit'
CHECKOUT = 'checkout'
DESCRIBE = 'describe'
GIT = 'git'
INIT = "init"
FETCH = 'fetch'
LOG = "log"
PUSH = 'push'
PULL = 'pull'
SHORTLOG = 'shortlog'
SHOW = 'show'
MV = 'mv'
RM = 'rm'
REFLOG = 'reflog'
REV_PARSE = 'rev-parse'
REV_LIST = 'rev-list'
RESET = 'reset'
REMOTE = 'remote'
STATUS = 'status'
TAG = 'tag'
S_ = '-s'
A_ = '-A'
GLOBAL_ = '--global'
USR_N = 'user.name'
USR_EMAIL = 'user.email'
# -------- bash -------------
DESCRYPTOR = " > "
DESCRYPTOR_ADD = " >> "
DES_ERR = " 2> "
DES_STD = " 1> "
LS = 'ls'
LS_LONG = '-laF'
# -------- files ------------
T_FILE = ".t_file"
