#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  vcsmanager.py
#
#   Testy oparte na razie na pomyśle wykorzystania słownika jako
# bazy dla komend i opcji.
#
# Copyright 2014 gumbi <pracacp@gmaill.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
##TODO: Uzupełnić dokumentacje klas i metod
#@TODO: poprawić opis help __doc__
"""Szablon wersja do obsługi przysłanych opcji z powłoki

    -h --help	:	wyświetla pomoc
    -o --output	:	output=cos
    -v		:	verbose
"""
import sys     # for arguments 
import getopt  # for handle arguments
import tools.log as log  # my custom logging class
import vcsconstans as vcsc

#@TODO: sprawdzić kolejność logowania metod


class VCSManager(dict):
    """
        Manager Version Control System class
    """
    language = ''

    def close_log(self):
        """
        Script without option python3 -O
        @return: None
        """

        if __debug__:
            self.objLog.close()

    def __init__(self, mod='git', lang='pl'):
        """Ctor

        @type self: dict
        """
        dict.__init__(self)

        if __debug__:
            self.objLog = log.MyLogger()
            self.objLog.log(self.__init__, ' In Ctor open log file')

        # settings for app language
        self['language'] = lang
        # settings for app Version Control System
        self['mode'] = '{0} repository'.format(mod)

    def set_language_version(self, lang):
        #TODO: Dopisać wersje językowe
        if __debug__:
            self.objLog.log(self.set_language_version, ' Sets application language %s' % lang)
        pass
        self.language = lang

    def commands(self, vcs):
        if __debug__:
            self.objLog.log(self.commands, ' Sets VCS commands for %s' % vcs)
        pass


class GITManager(VCSManager):
    """Subclass
        Git Manager Class
    """

    def __setitem__(self, key, value):
        if __debug__:
            self.objLog.log(self.__setitem__, ' Sets values for dict --- language and mode ---')

        if key == "language" and value:
            self.set_language_version(value)

        if key == 'mode' and value:
            self.commands(value)

        VCSManager.__setitem__(self, key, value)

    def commands(self, vcs):  # TODO: Dopisać patroszenie VCS z komend
        """

        @param vcs:
        """
        VCSManager.commands(self, vcs)
        self.update(vcsc.COMMANDS_GIT)


class HGManager(VCSManager):
    """Subclass
        Mercurial Manager Class
    """
    def __setitem__(self, key, value):
        if __debug__:
            self.objLog.log(self.__setitem__, ' Sets values for dict --- language and mode ---)')
        if key == "language" and value:
            self.set_language_version(value)
        if key == 'mode' and value:
            self.commands(value)
        VCSManager.__setitem__(self, key, value)


def class_manager(vcs='git', lang='pl'):
    """
    Funkcja tworzy i zwraca obiekt jednej z klas obsługującyh system wersionowania.
     Dostępne systemy wersjonowania to git i Mercurial. Klasy rozszeżają klasę słownika
     i zwracany obiekt jest słownikiem pythona.

    Function create and return object from one of manager class witch manage system control version.
    Returned object is just python dictionary.

    @param vcs: Version Control System for Git = 'git' , Mercurial = 'hg'
    @param lang: language application for english = 'en', polish = 'pl'
    @return: class object  '%sManager' % vcs
    """
    vcs = vcs.upper()

    def get_class(vcsClass, module=sys.modules[VCSManager.__module__]):
        """

        """
        nameClass = '%sManager' % vcsClass
        # if __debug__:
        #     print("In get_class method local variable type nameClass = ", type(nameClass))
        #     print("In get_class method local variable nameClass = ", nameClass)

        return hasattr(module, nameClass) and getattr(module, nameClass) or VCSManager

    return get_class(vcs)(vcs, lang)


def usage():
    """
        Function for printing help massage in shell
    """
    print(__doc__)


def main(arg):
    """
        Function main starts module
        In argv:

            @param: vcs Version Control System default git
            @param: lang language of application default polish
    """
    #TODO: Dokończyć testowanie
    try:
        opts, args = getopt.getopt(arg, 'h', ['help', 'lang=', 'vcs='])
    except getopt.GetoptError as e:
        # print help information and exit
        print(e)
        usage()
        sys.exit()

    lang = None
    vcs = None
    for o, a in opts:
        try:
            if o in ('--vcs',):
                if a == 'git' or a == 'hg':
                    vcs = a
                else:
                    vcs = 'git'
                    raise Warning('Just Git i Merciural is implemented,set default is Git.')
            elif o in ('--lang',):
                if a == 'pl' or a == 'en':
                    lang = a
                else:
                    lang = 'pl'
                    raise Warning('Just polish and english is implemented in application,'
                                  'set default language polish.')
            elif o in ('-h', '--help'):
                usage()
                sys.exit()
            else:
                assert False, 'unhandled option'
        except Warning as e:
            print(e)

    return class_manager(vcs, lang)


if __name__ == '__main__':
    argv = sys.argv[1:]
    if not argv:
        argv = ('--lang=pl', '--vcs=git')

    #logowane do pliku log.txt
    objCM = main(argv)
    print(type(objCM))
    print('\n'.join('%s = %s' % (k, v) for k, v in objCM.items()))
    objCM.close_log()


