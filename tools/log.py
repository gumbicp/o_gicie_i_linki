#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Klasa do logów
#  Copyright 2014 gumbi <pracacp@gmail.comc>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
__version__ = 0.1
__author__ = 'Gumbicp'
__copyright__ = 'Gumbicp'

import sys


class MyLogger(object):
    """
    Klasa do wprowadzania logów do pliku log.txt
    Pamiętaj o wywołaniu metody close() po skończonym logowaniu.
    """

    def __init__(self, mode='w'):
        """
            Ctor
        """
        self.saveout = sys.stdout
        self.fsock = None
        self._open_file_log(mode)

    def close(self):
        """
            Zamyka plik logu i przywraca strumień stdout do stanu wyjściowego
        """
        print("!!\n -- Zamknięcie pliku logu przez metodę %s() \n %s-- \n!!" % (self.close.__name__,
                                                                                self.close.__doc__))
        print(80*'#')
        sys.stdout = self.saveout
        self.fsock.close()
        
    def _open_file_log(self, mode='w', file='log.txt'):
        """
            Prywatna metoda otwierająca plik logu. Standardowo tworzy nowy plik.
            @param mode: 'w' write lub 'a' append
            @param file: plik logu
        """
        # otwieramy plik
        try:
            self.fsock = open(file, mode)
            sys.stdout = self.fsock
            if mode == 'w':
                print(80*'#')
                print("::--- %s ---::" % self.__doc__)
            elif mode == 'a':
                print("::---DODANE DO LOGU %s ---::" % self.__doc__)

        except IOError as e:
            print(':: -- BŁĄD W _open_log_file() KLASY MyLogger -> %s -- ::' % e)

    def log(self, method, txt=None):
        """
            Metoda drukuje do pliku logu.
            @param method: referencja do metody z której wołamy log()
            @param txt: opis co robi ipt
        """
        print("\n:: - Metoda:%s, %s \n::-> Co robi: %s - ::\n" % (method.__class__, method, txt))


#class Test:
    #"""
    #Klasa testowa
    #"""
    #objTL = MyLogger('a')

    #def testowa(self):
        #if __debug__:
            #self.objTL.log(self.testowa, 'w ifie __debug__')

    #def zamykaj_log(self):
        #self.objTL.close()

#if __name__ == '__main__':
    #obj = MyLogger()
    #obj.log('w if __name__ ', 'testy')
    #obj.close()
    #objT = Test()
    #objT.testowa()
    #objT.zamykaj_log()
