#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#
# gitmanager.py
#
#   Klasa Obsługi repozytoriów git
#
#  Copyright 2014 gumbi <pracacp@gmail.comc>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
__version__ = 0.1
__author__ = 'Gumbicp'
__copyright__ = 'Gumbicp'

import os
import constans as Gtp
import tools.log as log


class GitManager(object):
    _git_exist = False

    @staticmethod
    def git_clone(https):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    @staticmethod
    def if_git_exist():
        """
            Sprawdza czy w bierzącym katalogu jest repo gita

        @return: True/False -> jest/nie ma
        """
        if os.access(".git", os.F_OK):
            return True
        else:
            return False

    @staticmethod
    def create_gitignore(*args, mode="a"):
        """
            Dodaje plik .gitignore lub dodaje dane do niego zależnie
             od zmiennej mode
        @param args: co ma być ignorowane przez repo git
        @param mode: domyślnie ustawione na append do pliku
        """
        try:
            with open(".gitignore", mode) as f:
                for value in args:
                    f.write(value + "\n")
        except IOError as e:
            print(":: Create_gitignore --> ", e)

    def close_log(self):
        if __debug__:
            self.objLog.close()

    def __init__(self):
        """
            Ctor
            - Obsługa logu (jeżeli python -O gitmanager.py)
            - ustawienie zmiennych:
                self.zawartosc - zmienna dla pliku tymczasowego
                self._git_exist - czy repo istnieje w katalogu roboczym
        """
        if __debug__:
            self.objLog = log.MyLogger()

        self.zawartosc = ""
        if GitManager.if_git_exist():
            self._git_exist = True
        else:
            self._git_exist = False

    def git_init(self):
        """
        Inicjalizuje repo git w bierzącym katalogu

        @return: def read_t_file
        @raise Warning: jeżeli git już istnieje
        """
        if self._git_exist:
            raise Warning("Repo gita już istnieje w tym folderze !!!")
        else:
            os.system(Gtp.GIT + Gtp.SEP + Gtp.INIT + Gtp.DESCRYPTOR + Gtp.T_FILE)
            return self.read_t_file()

    def git_config(self, userName=None, email=None, **kargv):
        """
            Funkcja konfiguracji repo git.

        """
        #TODO: Dopracować **kargv
        _t_string = ""
        if self._git_exist:
            if userName:
                os.system(
                    Gtp.GIT + Gtp.SEP + Gtp.CONFIG + Gtp.SEP + Gtp.GLOBAL_ + Gtp.SEP + Gtp.USR_N + Gtp.SEP + userName +
                    Gtp.DESCRYPTOR + Gtp.T_FILE)
                _t_string = "echo 'Config User name. Done'"
                if email is None:
                    return self.read_t_file()
            if email:
                os.system(
                    Gtp.GIT + Gtp.SEP + Gtp.CONFIG + Gtp.SEP + Gtp.GLOBAL_ + Gtp.SEP + Gtp.USR_EMAIL + Gtp.SEP + email +
                    Gtp.DESCRYPTOR + Gtp.T_FILE)
                _t_string += " 'Config email. Done'"
            #return self.read_t_file()
            if len(kargv) != 0:
                print(kargv)
                key = kargv['l']
                os.system(_t_string + Gtp.SEP + Gtp.DESCRYPTOR_ADD + Gtp.T_FILE)
                os.system(Gtp.GIT + Gtp.SEP + Gtp.CONFIG + Gtp.SEP + key + Gtp.SEP + Gtp.DESCRYPTOR_ADD + Gtp.T_FILE)
                return self.read_t_file()
            elif userName or email:
                os.system(_t_string + Gtp.SEP + Gtp.DESCRYPTOR_ADD + Gtp.T_FILE)
                return self.read_t_file()
            else:
                return None
        else:
            return None

    def git_add(self, *args, **kargs):
        #TODO rozbudowa o przyslany slownik opcji
        """
            Komenda  git add.
        @param args: np Gtp.A_ (-A) lub plik plik plik
        @param kargs: przysłany słownik klucz : opcja
        @return:
        """
        if self._git_exist:
            dl_arg = len(args)
            if dl_arg == 1:
                os.system(Gtp.GIT + Gtp.SEP + Gtp.ADD + ' -v ' + args[0] + Gtp.DESCRYPTOR + Gtp.T_FILE)
                return self.read_t_file()
        else:
            return None

    def git_commit(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_branch(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_rm(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_mv(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_checkout(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_reset(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_archive(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_remote(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_push(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_pull(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_tag(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_describe(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_status(self, opcja=None):

        """
            Komenda git status
        @param opcja: np Gtp.S_ (-s) lub None
        @return: zawartość T_FILE
        """
        if self._git_exist:
            if opcja:
                os.system(Gtp.GIT + Gtp.SEP + Gtp.STATUS + Gtp.SEP + opcja +
                          Gtp.DESCRYPTOR + Gtp.T_FILE)
                return self.read_t_file()
            else:
                os.system(Gtp.GIT + Gtp.SEP + Gtp.STATUS + Gtp.DESCRYPTOR + Gtp.T_FILE)
                return self.read_t_file()
        else:
            return None

    def git_log(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_shortlog(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_show(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_reflog(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_rev_parse(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def git_rev_list(self):
        #TODO implementacja
        print("Not implemented yet...")
        pass

    def read_t_file(self):
        """
            Czyta tymczasowy plik i zwraca jego zawartość

        @return: zawartość T_FILE
        """
        try:
            with open(Gtp.T_FILE, 'r') as f:
                self.zawartosc = f.read()
                f.close()
                return self.zawartosc
        except IOError as e:
            print(Gtp.T_FILE, " - nie ma takiego pliku !!!", e)


def clear_temp_files():
    """
        Funkcja czysci tymczasowe pliki.
        -T_FILE

    """
    os.system("rm -fv " + Gtp.T_FILE)

# do testów modulu

if __name__ == "__main__":
    obj = GitManager()
    #try:
    #    print(obj.git_init())
    #except Warning as _e:
    #    print(_e)
    #GitManager.create_gitignore(Gtp.T_FILE)
    #print(obj.git_add(Gtp.A_))
    #print(obj.git_status())
    #print(obj.git_config("Gumbicp", "pracacp@gmail.com", l="--list"))
    #print(obj.git_config("Gumbicp", "pracacp@gmail.com"))
    # czyszczenie śmiecia

    clear_temp_files()
